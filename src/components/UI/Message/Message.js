import React from 'react';
import './Message.css';

const Message = props => {
  return (
    <div
      className={["Message", props.type].join(' ')}
      style={{ opacity: props.show ? '1' : '0' }}
    >{props.message}</div>
  );
};

export default Message;