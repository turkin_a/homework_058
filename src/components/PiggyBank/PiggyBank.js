import React from 'react';
import './PiggyBank.css';
import Button from "../UI/Button/Button";
import Message from "../UI/Message/Message";

const PiggyBank = props => {
  const drops = props.coins.map(coin => {
    const imgURL = `img/${coin.id}som.png`;

    return (
      <div className="Drop" key={coin.id}>
        <div className="CoinImg">
          <img src={imgURL} alt=""/>
        </div>
        <span className="TotalCoins">Total: {coin.value} coins</span>
        <Button type="Success" label="Push a coin" clicked={() => props.addCoin(coin.id)}/>
      </div>
    );
  });

  return (
    <div className="PiggyBank">
      <h2 className="Title">Piggy Bank</h2>
      <div className="Summary">
        <div className="PiggyImg">
          <img src="img/piggy-bank.png" alt=""/>
        </div>
        <div className="Total">Total sum in Piggy: <strong>{props.totalSum} som</strong></div>
        <Button type="Success" label="Pour it into the pocket" clicked={() => props.pouring()}/>
        <Message
          type={props.messageType}
          message={props.messageType === 'Success' ? 'All coins pour into the pocket' : 'Piggy is empty'}
          show={props.show}
        />
      </div>
      <div className="DropBox">
        {drops}
      </div>
      <div className="Pocket">Total sum in pocket: <strong>{props.sumInPocket} som</strong></div>
    </div>
  );
};

export default PiggyBank;