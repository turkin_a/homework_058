import React, { Component } from 'react';
import './App.css';
import PiggyBank from "../components/PiggyBank/PiggyBank";
import Wrapper from "../hoc/Wrapper";
import Modal from "../components/UI/Modal/Modal";
import Button from "../components/UI/Button/Button";

class App extends Component {
  state = {
    coins: [
      {id: 1, value: 0},
      {id: 3, value: 0},
      {id: 5, value: 0},
      {id: 10, value: 0}
    ],
    totalSum: 0,
    sumInPocket: 0,
    pourable: false,
    pouring: false,
    messageType: 'Danger',
    showMessage: false
  };

  addCoin = (id) => {
    const index = this.state.coins.findIndex(c => c.id === id);
    const coins = [...this.state.coins];
    const coin = {...coins[index]};

    coin.value++;
    coins[index] = coin;

    this.setState({coins});
    this.calculateSum(coins);
  };

  pourAll = () => {
    const coins = [...this.state.coins];
    const sumInPocket = this.state.sumInPocket + this.state.totalSum;

    const newCoins = coins.map((el, index) => {
      const coin = {...coins[index]};
      coin.value = 0;
      coins[index] = coin;
      return coins[index];
    });

    this.setState({coins: newCoins, totalSum: 0, sumInPocket, pourable: false});
    this.handleClosePouring();
    this.showMessage('Success');
  };

  handlePouring = () => {
    const pouring = this.state.pourable;

    this.setState({pouring});

    if (!pouring) this.showMessage('Danger');
  };


  handleClosePouring = () => {
    this.setState({pouring: false});
  };

  calculateSum = (coins) => {
    const totalSum = coins.reduce((sum, coin) => sum + coin.id * coin.value, 0);

    this.setState({totalSum, pourable: totalSum > 0});
  };

  showMessage = (messageType) => {
    this.setState({messageType});

    setTimeout(() => {
      this.setState({showMessage: true});
    }, 0);

    setTimeout(() => {
      this.setState({showMessage: false});
    }, 1500)
  };

  render() {
    return (
      <Wrapper>
        <Modal
          show={this.state.pouring}
        >
          <div className="ModalTitle">Pour to pocket</div>
          <div className="ModalBody">Do you want to pour all coins into the pocket?</div>
          <Button
            type={'Continue'}
            label={'OK'}
            clicked={this.pourAll}
          />
          <Button
            type={'Danger'}
            label={'Cancel'}
            clicked={this.handleClosePouring}
          />
        </Modal>
        <div className="App">
          <PiggyBank
            coins={this.state.coins}
            totalSum={this.state.totalSum}
            sumInPocket={this.state.sumInPocket}
            addCoin={this.addCoin}
            pouring={this.handlePouring}
            messageType={this.state.messageType}
            show={this.state.showMessage}
          />
        </div>
      </Wrapper>
    );
  }
}

export default App;
